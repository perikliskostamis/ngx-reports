// tslint:disable-next-line:max-line-length
import { Component, ElementRef, EventEmitter, Input, OnDestroy, AfterViewInit, Output, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { SignerService } from '@universis/ngx-signer';
import {
  AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, PageInfo
} from '@universis/ngx-tables';
import { FormioComponent, FormioRefreshValue } from 'angular-formio';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { Observable, Subject, Subscription } from 'rxjs';
import { ReportService } from '../../services/report.service';
import { REPORT_LIST } from './select-report.list.config';

declare var $: any;

declare interface SelectedReport {
  id: number;
  name: string;
  reportCategory?: any;
  dateCreated?: any;
  dateModified?: any;
  signReport?: boolean;
  url?: string;
  useDocumentNumber?: boolean;
  documentSeries?: any;
}

@Component({
  selector: 'app-select-report',
  templateUrl: './select-report.component.html',
  styleUrls: ['./select-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectReportComponent extends RouterModalOkCancel implements AfterViewInit, OnDestroy {

  @Input('appliesTo') appliesTo: string;
  @ViewChild('nameTemplate') nameTemplate: ElementRef;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('form') form: FormioComponent;
  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;
  @ViewChild('filterDropdown') filterDropdown: ElementRef;
  public tableConfig: AdvancedTableConfiguration;
  public searchText: string;
  public loading: boolean;
  public recordsTotal = 0;
  public recordsFiltered = 0;
  public pageInfo = <PageInfo> {
    serverSide: true
  };
  public selectedReport: SelectedReport;
  private selectedItemsSubscription: Subscription | any;
  private dataSubscription: Subscription | any;
  private statusSubscription: Subscription;
  public reportForm: any;
  @Output() refreshForm: EventEmitter<FormioRefreshValue> = new EventEmitter<FormioRefreshValue>();
  public blob: any;
  @Input('item') item: any;
  public lastError: any;
  public formData: any = {};
  public signatures: File[] = [];
  public formProperties: any = {};
  public filterValues = {
    defaultFilterValue: 'AllReportCategories',
    reportCategories: [],
  };
  public firstLoad = true;
  public modalButtonsSubject: Subject<string>; // Passes the user actions to report-print
  public modalButtons$: Observable<string>;
  public formConfig;
  private  documentSeriesUrl;

  constructor(router: Router, activatedRoute: ActivatedRoute,
              private _reports: ReportService,
              private _signer: SignerService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _translateService: TranslateService
    ) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Reports.Print';
    // disable ok button
    this.okButtonDisabled = true;
    // set button text
    this.okButtonText = 'Reports.NextButton';
  }
  ngOnDestroy(): void {
    if (this.selectedItemsSubscription) {
      this.selectedItemsSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.statusSubscription) {
      this.statusSubscription.unsubscribe();
    }
    this._signer.destroy();
  }

  ngAfterViewInit() {
    const columnTemplate = `
        <span class='d-flex p-2'><i class='fa text-indigo fa-file fa-3x'></i>
          <div class='ml-3 justify-content-center'>
            <div class="name">$\{name\}</div>
            <div class='text-gray-500 details'>$\{reportCategoryName\}</div>
          </div>
          <div class='ml-auto align-self-center'>
            <i class="fas fa-2x text-white fa-check-circle"></i>
          </div>
        </span>
    `;
    const tableConfig = AdvancedTableConfiguration.cast(REPORT_LIST);
    const findColumn = tableConfig.columns.find( column => {
      return column.name === 'name';
    });
    if (findColumn) {
      findColumn.formatString = columnTemplate;
    }
    this.selectedItemsSubscription = this.table.selectedItems.subscribe((selectedItems: Array<any>) => {
      this.okButtonDisabled = selectedItems.length === 0;
    });
    this.okButtonDisabled = true;
    // get data
    this.dataSubscription = this.activatedRoute.data.subscribe( (data: any) => {
        try {
          // get model
          const model = data.model;
          // validate model
          if (model == null) {
            throw new Error('Current model cannot be empty at this context');
          }
          // get metadata
          return this._context.getMetadata().then( schema => {
            // get entity type
            const entitySet = schema.EntityContainer.EntitySet.find( x => {
                return x.Name === model;
            });
            // entity type cannot be null
            // todo: show message
            if (entitySet == null) {
              // set empty query
              this.table.query = this._context.model('ReportTemplates')
                  .where('id').equal(null)
                  .prepare();
            } else {
              // set query based on current  entity type
              this.table.query = this._context.model('ReportTemplates')
                  .where('reportCategory/appliesTo').equal(entitySet.EntityType)
                  .prepare();
            }
            // get item assigned to this operation
            this.item = data.item;
            this.documentSeriesUrl = data.documentSeriesUrl;
            this.formProperties = {
              ID: this.item.id,
              documentSeriesUrl: this.documentSeriesUrl
            };

            // finally set table configuration
            this.table.config = tableConfig;
            // fetch data
            this.table.fetch();
          }).catch (err => {
            console.error(err);
            this._errorService.showError(err, {
              continueLink: '.'
            });
            this.close();
          });
        } catch (err) {
          console.error(err);
          this.close().then(() => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
    });

    this.modalButtonsSubject = new Subject();
    this.modalButtons$ = this.modalButtonsSubject.asObservable();
  }

  onRowSelect(row: any) {
    this.table.toggleSelectRow(row);
  }

  onFormValidation(event: any) {
    // handle changes (check if event has isValid property)
    if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
      // enable or disable button based on form status
      this.okButtonDisabled = !event.isValid;
    }
    if (event.srcElement && event.srcElement.name === 'data[signReport]') {
      if (this.form.formio.submission.data.signReport) {
        // do some extra things
      }
    }
  }

  onKeyUp(event) {
    if (event.keyCode === 13) {
      this.okButtonDisabled = true;
      this.table.search(this.searchText);
    }
  }

  onFilterClick(event) {
    // get value
    const value = event.target.innerHTML.trim();
    // search text
    value === this._translateService.instant(this.filterValues.defaultFilterValue) ? this.table.search(' ') : this.table.search(value);
    // update button text
    this.filterDropdown.nativeElement.innerHTML = `<i class="fa fa-sliders-h pr-2 text-secondary"></i> ${value} `;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.recordsFiltered = data.recordsFiltered;
    // get first page info (because page is not going to be fired for first page)
    if (data.recordsTotal > 0 && this.pageInfo.recordsTotal === 0) {
      this.pageInfo.recordsTotal = data.recordsTotal;
    }
    if (this.firstLoad) {
      // get templates
      const templates = data.data;
      // populate filter with distinct report categories
      templates.forEach(template => {
        const reportCategoryName = template['reportCategoryName'];
        if (this.filterValues.reportCategories.indexOf(reportCategoryName) < 0) {
          this.filterValues.reportCategories.push(reportCategoryName);
        }
      });
      // push default filter value
      this.filterValues.reportCategories.unshift(this.filterValues.defaultFilterValue);
      this.firstLoad = false;
    }
  }

  async ok(): Promise<any> {
    if (this.selectedReport == null) {
      // set selected report
      const selected = this.table.selected[0];
      const selectedReport = await this._reports.getReport(selected.id);
      Object.assign(selectedReport, {
        signerServiceStatus: -1
      });
      const { inputControls } = await this._reports.getReportData(selected.id);
      const report = await this._reports.getReport(selected.id);
      if (inputControls) {
        this.formConfig = await this._reports.generateVariablesForm(inputControls);
      }
      this.selectedReport = selectedReport;
      // get report form
      const params = {
        documentSeriesUrl: this.documentSeriesUrl
      };
      const form = await this._reports.getReportFormFor(this.selectedReport, params);
      // set report form with no data
      this.reportForm = form;
      // set button text
      this.okButtonText = 'Reports.PrintButton';
      // check signer service
      // subscribe for signer status
      this.lastError = null;
      return true;
    } else {
      this.modalButtonsSubject.next('submit');
      return false;
    }
  }

  cancel(): Promise<any> {
    return this.close();
  }
}
