 export const DOCUMENTS_LIST_CONFIG = {
  "model": "DocumentNumberSeriesItems",
  "title": "Documents",
  "searchExpression":"indexof(documentNumber, '${text}') ge 0 or indexof(description, '${text}') ge 0",
  "columns": [
    {
      "name": "documentNumber",
      "title": "Documents.DocumentNumber"
    },
    {
      "name": "description",
      "title": "Documents.Description"
    },
    {
      "name": "documentCode",
      "title": "Documents.DocumentCode",
      "formatter": "TemplateFormatter",
      "formatString": "${documentCode}",
      "hidden": true
    },
    {
      "name": "dateCreated",
      "title": "Documents.CreatedAt",
      "formatter": "DateTimeFormatter",
      "formatString": "short"
    },
    {
      "name": "dateModified",
      "title": "Documents.ModifiedAt",
      "formatter": "DateTimeFormatter",
      "formatString": "short"
    },
    {
      "name": "url",
      "hidden": true
    },
    {
      "name": "signed",
      "title": "Documents.Signed",
      "formatter": "TrueFalseFormatter",
      "className": "text-center"
    },
    {
      "name": "published",
      "title": "Documents.Published",
      "formatter": "TrueFalseFormatter",
      "className": "text-center"
    },
    {
      "name": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
          "buttonContent": "<i class=\"fas fa-2x fa-file-download text-indigo\"></i>",
          "buttonClass": "btn btn-default",
          "commands": [ "." ],
          "navigationExtras": {
            "replaceUrl": false,
            "queryParams": {
              "download": "${documentCode}"
            }
          }
      }
  }
  ],
  "defaults": {
    "orderBy": "dateModified desc,dateCreated desc"
  }
}

